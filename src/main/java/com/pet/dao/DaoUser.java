package com.pet.dao;

import java.util.List;

import com.pet.model.User;
import com.pet.model.MyCart;
import com.pet.model.PetDetails;

public interface DaoUser {
	public void userLogin(User user);

	public boolean checkLogin(String userName, String Password);

	public List<PetDetails> viewpets();

	public void addpets(PetDetails petdetails);

	public List<PetDetails> getByPetName(PetDetails petdetails);

	public void insertIntoMyPets(MyCart mypets);

	public List<MyCart> viewmypets();

}
